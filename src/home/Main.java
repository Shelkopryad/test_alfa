package home;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Shelk on 1/23/2018.
 */
public class Main {

    public static void main(String[] args) {
        List<String> list = Helper.readFile("src/res/digits.txt");
        list.forEach(n -> System.out.println(n));
        System.out.println();

        List<String> letters = Arrays.asList(list.get(0).split(","));
        List<Integer> digits = Helper.lettersToDigits(letters);
        System.out.println(Helper.sort(digits, true).toString());
        System.out.println(Helper.sort(digits, false).toString());
        System.out.println();

        System.out.println(Helper.factorial(20));
        System.out.println(Helper.getFactorialList(10).toString());
    }
}
