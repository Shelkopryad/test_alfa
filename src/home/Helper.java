package home;

import java.io.*;
import java.util.*;

/**
 * Created by Shelk on 1/23/2018.
 */
class Helper {

    private static FileReader fileReader;
    private static Scanner scanner;

    static List<String> readFile(String path) {
        List<String> list = new ArrayList<>();
        try {
            fileReader = new FileReader(path);
            scanner = new Scanner(fileReader);
            while (scanner.hasNext()) {
                list.add(scanner.next());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Упс, а файла-то и нет");
        }
        return list;
    }

    static List<Integer> lettersToDigits(List<String> letters) {
        List<Integer> digits = new ArrayList<>();
        letters.forEach(n -> digits.add(Integer.parseInt(n)));
        return digits;
    }

    static List<Integer> sort(List<Integer> list, boolean reverse) {
        if (reverse) {
            Collections.sort(list);
        } else {
            Collections.sort(list, Collections.reverseOrder());
        }
        return list;
    }

    static List<Long> getFactorialList(int size) {
        List<Long> result = new ArrayList<>();

        for (int i = 0; i <= size; i++) {
            result.add(factorial(i));
        }
        return result;
    }

    static long factorial(int n) {
        return n == 0 ? 0 : n == 1 ? 1 : n * factorial(n - 1);
    }
}
